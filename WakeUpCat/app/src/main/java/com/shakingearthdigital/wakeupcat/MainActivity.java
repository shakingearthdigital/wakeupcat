package com.shakingearthdigital.wakeupcat;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements VolumeUtil.OnVolumeSampleListener {

    private static final String TAG = "MainActivity";

    private FrameLayout mContainer;

    //    private List<ImageView> mYarn = new ArrayList<>();
    private int[] mYarnColors = {0xff00ddff, 0xff669900,
            0xffaa66cc, 0xffff8800, 0xffcc0000};
    private int mYarnSize;

    private Random mRandom = new Random();
    private int mYarnXMax;

    private Handler mHandler;

    private VolumeUtil mVolumeUtil;
    private View mCat;
    private TextView mScoreView;
    private int mScore;

    private Button startButton;

    private int[] filterVolumes = {0, 0, 0};
    private int volumeIndex = 0;

    private int mFallDuration = 4000;

    private boolean mGameOver = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mYarnSize = (int) getResources().getDimension(R.dimen.yarn);

        mScoreView = (TextView) findViewById(R.id.score);

        mContainer = (FrameLayout) findViewById(R.id.gameContainer);

        mCat = mContainer.findViewById(R.id.cat);
        mCat.setVisibility(View.GONE);

        mYarnXMax = getResources().getDisplayMetrics().widthPixels - mYarnSize;

        mHandler = new Handler();

        startButton = (Button) findViewById(R.id.gameButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void startGame() {
        mGameOver = false;
        mScore = 0;
        mScoreView.setText(String.valueOf(mScore));
        mCat.setVisibility(View.VISIBLE);
        findViewById(R.id.gameContainer).setVisibility(View.VISIBLE);
        findViewById(R.id.uiContainer).setVisibility(View.GONE);

        mHandler.postDelayed(dropYarnRunnable, 3000);

        mVolumeUtil = new VolumeUtil(this);

        mVolumeUtil.startAudioInput();

        startButton.setVisibility(View.GONE); //Get rid of game button
    }

    private ImageView createYarn(ViewGroup parent) {
        Log.d(TAG, "createYarn");
        final ImageView view = new ImageView(this);
        view.setLayoutParams(new FrameLayout.LayoutParams(mYarnSize, mYarnSize));
        view.setImageResource(R.mipmap.yarn);
        parent.addView(view);
        view.setX(mRandom.nextInt(mYarnXMax));
        view.setY(-mYarnSize);
        view.setColorFilter(mYarnColors[mRandom.nextInt(mYarnColors.length - 1)], PorterDuff.Mode.MULTIPLY);
        view.animate().setInterpolator(new LinearInterpolator())
                .y(parent.getHeight())
                .rotation(mRandom.nextInt(1080))
                .setDuration(mFallDuration)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                        Log.d(TAG, "createYarn : onAnimationStart");
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Log.d(TAG, "createYarn : onAnimationEnd");
                        mContainer.removeView(view);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).setUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if (mGameOver) view.animate().cancel();

                Log.d(TAG, String.format("cat top=%f left=%f yarn bottom=%f left=%f", mCat.getY(), mCat.getX(), view.getY(), view.getX()));
                if (view.getY() > mCat.getY() && Math.abs(mCat.getX() - view.getX()) < mYarnSize) {
                    Log.d(TAG, "Cat catches ball!");
                    updateScore();
                    view.animate().cancel();
                }
            }
        })

                .start();
        return view;
    }

    private void updateScore() {
        mScore += 500; //Add 500 to the score
        mScoreView.setText(String.valueOf(mScore));

        if (mScore == 10000) { //If score is equal to 1000
            mGameOver = true;

            findViewById(R.id.gameContainer).setVisibility(View.GONE);
            mVolumeUtil.stopAudioInput();                           //Stop audio input
            mCat.setVisibility(View.GONE);                          //Hide Game Cat
            findViewById(R.id.winCat).setVisibility(View.VISIBLE);  //Show Win Cat
            findViewById(R.id.gameButton).setVisibility(View.VISIBLE);
            startButton.setText("Replay");                          //Change Game button to say 'replay'
            findViewById(R.id.uiContainer).setVisibility(View.VISIBLE); //Show UI Container
        }
    }

    private Runnable dropYarnRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mGameOver) {
                createYarn(mContainer);
                mFallDuration = Math.max(600, mFallDuration - 200);
                mHandler.postDelayed(this, 2000);
            }
        }
    };

    @Override
    public void onVolumeSample(int volume) {
        filterVolumes[volumeIndex % 3] = volume;
        volumeIndex++;

        float useVolume = (filterVolumes[0] + filterVolumes[1] + filterVolumes[2]) / 3f;

        mCat.setX(useVolume * .035f);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVolumeUtil.onDestroy();
    }
}
