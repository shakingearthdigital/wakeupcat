package com.shakingearthdigital.wakeupcat;

import android.media.MediaRecorder;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Jens on 12/8/2015.
 */
public class VolumeUtil {
    private static final String TAG = "VolumeUtil";
    private static final long AUDIO_SAMPLE_DELAY = 50;

    private MediaRecorder mRecorder;
    private Handler mHandler;

    public interface OnVolumeSampleListener{
        void onVolumeSample(int volume);
    }
    private final OnVolumeSampleListener volumeListener;

    public VolumeUtil(OnVolumeSampleListener listener){
        volumeListener = listener;
    }


    public void startAudioInput() {
        Log.d(TAG, "startAudioInput");

        if (mHandler == null){
            mHandler = new Handler();
        }

        if (mRecorder == null) {
            try {
                mRecorder = new MediaRecorder();
                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                mRecorder.setOutputFile("/dev/null");
                mRecorder.prepare();
                mRecorder.start();
                mHandler.postDelayed(inputVolumeRunnable, AUDIO_SAMPLE_DELAY);
            } catch (IOException e) {
                Log.e(TAG, "startAudioInput",e);
            }
        }
    }

    private Runnable inputVolumeRunnable = new  Runnable() {

        @Override
        public void run() {
            if (mRecorder != null) {
                int amplitude = mRecorder.getMaxAmplitude();
//                Log.d(TAG, "volume=" + amplitude);// / 2700.0

                volumeListener.onVolumeSample(amplitude);

                mHandler.postDelayed(this, AUDIO_SAMPLE_DELAY);
            }
        }
    };

    public void stopAudioInput() {
        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
    }

    public void onDestroy() {
        stopAudioInput();
    }
}
